/*

2) GAL Report - we would like to create the ability to see what web and leads that an agent is able to pull at a given point of time. 
The report will display the leads that an agent is able to pull, the total number of accounts. columns will include, 
Agent, Account ID, Leads, Campaign, Tier and Create Date

*/

USE [SelectCARE_SQL]
GO

/****** Object:  StoredProcedure [dbo].[GetWebGALAgentListByAccountID]    Script Date: 11/14/2018 11:18:42 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Yasir A.
-- Create date: 02 Sep, 2014
-- Description:	Get the List of Web GAL users to notify against account ID
-- =============================================
CREATE PROCEDURE [dbo].[GetWebGALAgentListByAccountID]
	@AccountKey Bigint = 0
AS
BEGIN
	SET NOCOUNT ON;
	    
	DECLARE @DST INT ,
            @StartHour INT ,
            @EndHour INT ,
            @D AS DATETIME

        SET @D = GETDATE()

        IF ( SELECT OptionValue
             FROM   gal_SystemOptions
             WHERE  OptionName = 'DST'
           ) LIKE 'True'
            SET @DST = 1
        ELSE
            SET @DST = 0

        SELECT  @StartHour = ISNULL(( SELECT    OptionValue
                                      FROM      gal_SystemOptions
                                      WHERE     OptionName = 'TZStartHour'
                                    ), 9)
        SELECT  @EndHour = ISNULL(( SELECT  OptionValue
                                    FROM    gal_SystemOptions
                                    WHERE   OptionName = 'TZEndHour'
                                  ), 20)
SELECT Distinct gal_agents.agent_id				
        FROM    gal_leads_new gal_leads ( NOLOCK )
                JOIN 
				campaigns (NOLOCK) ON campaigns.cmp_id = gal_leads.cmp_id
                                           AND campaigns.cmp_delete_flag = 0 AND campaigns.cmp_main_campaign_type = 1 --WEB
                JOIN gal_campaigns (NOLOCK) ON gal_leads.cmp_id = gal_campaigns.campaign_id
                                               AND campaign_inactive = 0
                JOIN gal_CampaignGroups (NOLOCK) ON campaign_campaign_group_id = campaign_group_id
                                                    AND campaign_group_delete_flag = 0
                                                    AND IsNull(campaign_group_acd_flag,0) = 0
                                                    AND campaign_group_inactive = 0
				join 
				vw_EvaluateWebDashBoardStats temp On temp.campaign_group_id = gal_CampaignGroups.campaign_group_id
                
                JOIN gal_Agents (NOLOCK) ON temp.agent_id = dbo.gal_agents.agent_id
                LEFT JOIN ( SELECT  gal_AgeGroup2AgentGroup.* ,
                                    gal_AgeGroups.* ,
                                    gal_Agents.agent_id
                            FROM    gal_AgeGroup2AgentGroup (NOLOCK)
                                    JOIN gal_AgeGroups (NOLOCK) ON agegrp2agtgrp_age_group_id = age_group_id
                                    JOIN gal_AgentGroups (NOLOCK) ON agegrp2agtgrp_agent_group_id = agent_group_id
                                    JOIN dbo.gal_agent2agentgroups aag ( NOLOCK ) ON aag.agent_group_id = gal_AgentGroups.agent_group_id
                                    JOIN gal_Agents (NOLOCK) ON aag.agent_id = gal_Agents.agent_id
                          ) ag2ag ON DATEDIFF(HOUR, indv_birthday, GETDATE())
                                     / 8766.0 BETWEEN age_group_start
                                              AND     age_group_end
                                     AND ag2ag.agent_id = gal_agents.agent_id
                LEFT JOIN gal_States (NOLOCK) ON sta_abbreviation = state_code
                LEFT JOIN gal_StateGroupStates (NOLOCK) ON stgrp_state_id = state_id
                LEFT JOIN gal_StateGroups (NOLOCK) ON stgrp_state_group_id = state_group_id
                LEFT JOIN ( SELECT  gal_StateGroup2AgentGroup.* ,
                                    gal_Agents.agent_id
                            FROM    gal_StateGroup2AgentGroup (NOLOCK)
                                    JOIN gal_AgentGroups (NOLOCK) ON agent_group_id = stgrp2agtgrp_agent_id
									AND gal_AgentGroups.agent_group_delete_flag = 0
                                               AND Isnull(gal_AgentGroups.agent_group_acd_flag,0) = 0
                                               --Irshad K [26 April 2016] Add a GAL group schedule to make a group active/inactive
											   AND dbo.GetAgentGroupEligibility(gal_AgentGroups.agent_group_active_days)=1
											   --AND gal_AgentGroups.agent_group_inactive = 0
                                    JOIN dbo.gal_agent2agentgroups aag ( NOLOCK ) ON aag.agent_group_id = gal_AgentGroups.agent_group_id
                                    JOIN gal_Agents (NOLOCK) ON aag.agent_id = gal_Agents.agent_id
                          ) sg2ag ON stgrp2agtgrp_state_id = state_group_id
                                     AND sg2ag.agent_id = gal_Agents.agent_id 
                LEFT JOIN state_licensure (NOLOCK) ON sta_key = stl_sta_key
                                                      AND stl_usr_key = gal_agents.agent_id
                LEFT JOIN gal_TimeZones TZI ( NOLOCK ) ON state_tz_id = TZI.tz_id
				join ( select distinct UserKey from SignalR_Central_Bindings ) SCB On gal_agents.agent_id = SCB.UserKey and Len(UserKey) > 31
        WHERE   
		--dbo.[PVStatusExclude](gal_agents.agent_id) = 0
		--AND
        dbo.[PVScheduleResult](gal_agents.agent_id) != 0
					AND
		( stgrp2agtgrp_priority IS NULL
                  OR stgrp2agtgrp_priority > 0
                )
                AND ( agegrp2agtgrp_priority IS NULL
                      OR agegrp2agtgrp_priority > 0
                    )
                AND ( sta_key IS NULL
                      OR stl_key IS NOT NULL
                    )
                AND ( ( TZI.tz_id IS NULL )
                      OR (@DST = 1 and datepart(hh, @D) + coalesce(null, TZI.tz_increment_dst)  >= @StartHour and datepart(hh, @D) + coalesce(null, TZI.tz_increment_dst) < @EndHour)
              OR (@DST = 0 and datepart(hh, @D) + coalesce(null, TZI.tz_increment_ost)  >= @StartHour and datepart(hh, @D) + coalesce(null, TZI.tz_increment_ost) < @EndHour)
                      --OR DATEDIFF(hh, act_add_date, @D) <= 3
                    )
                AND ( ( temp.MinLevel = 1
                        AND DATEADD(SECOND, ISNULL(temp.NextRefresh, 0),
                                    act_add_date) <= GETDATE()
                      )
                      OR ( temp.MinLevel = 2
                           AND DATEADD(SECOND,
                                       ISNULL(temp.NextRefresh, 60),
                                       act_add_date) <= GETDATE()
                         )
                      OR ( temp.MinLevel = 3
                           AND DATEADD(SECOND,
                                       ISNULL(temp.NextRefresh, 120),
                                       act_add_date) <= GETDATE()
                         )
                      OR ( temp.MinLevel = 4
                           AND DATEADD(SECOND,
                                       ISNULL(temp.NextRefresh, 180),
                                       act_add_date) <= GETDATE()
                         )
                    )		
					AND gal_leads.act_key = @AccountKey
        GROUP BY gal_leads.act_key,gal_agents.agent_id
		
END

GO






SELECT * FROM users
[dbo].[campaigns]
[dbo].[gal_agegroup2agentgroup]
[dbo].[gal_agegroups]
[dbo].[gal_agent2agentgroups]
[dbo].[gal_agentgroups]
[dbo].[gal_agents]
[dbo].[gal_campaigngroups]
[dbo].[gal_campaigns]
[dbo].[gal_stategroup2agentgroup]
[dbo].[gal_stategroups]
[dbo].[gal_stategroupstates]
[dbo].[gal_states]
[dbo].[gal_systemoptions]
[dbo].[gal_timezones]
[dbo].[Signal¬R_¬Central_¬Bindings]
[dbo].[state_licensure]
[dbo].[gal_leads_new]
[dbo].[vw_¬Evaluate¬Web¬Dash¬Board¬Stats]
[dbo].[PVSchedule¬Result]
