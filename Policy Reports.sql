/*
8)  Policies Quoted - We would like to create the ability to monitor the total number of customers that 
were quoted within a designated amount of time. We would like the ability to filter from 
within last hour, last 4 hours, current day, week, set the date range, last week, current month, last month and state. 
Columns: Quoting Agent, Account ID, First Name, Last Name, Campaign SelectCARE Status, Quote Date, Last Action


9)  Policies Sold - We would like the ability report on accounts that were sold within SelectCARE. 
This information will be used to show progress for current month, day, or year to date. 
We check examples within accounts within SelectCARE. This report will also be used to show how many policies stayed 
quoted with the ability to filter by skill group, agent, and date, state. Columns: Columns will include 
Agent Name, Skill Group, Account ID, Last Action, Last Action Date, Current Status

*/
# Campaign Status, Needs Last Action and Last Action Date and Skill Group

#Need to add the following index
#ALTER TABLE `p12life_dev`.`salesledger` ADD INDEX `ix_ref_number` (`refer_numb` ASC);


SELECT u.`Name` AS `Agent Name`, q.ClientID AS `Client ID`, pt.FirstName, pt.LastName, a.act_key AS `Account ID`,
	q.`Status` AS `Quote Status`, q.substatus AS `Quote Sub Status`, q.RequestDate AS `Quote Date`,
    s.Date_Time AS `Sale Date`
FROM p12life_dev.quote q
#INNER JOIN p12life_dev.policy p ON q.ReferenceNumb = p.ReferenceNumb
LEFT JOIN sclife_dev.arc_cases ac 
		INNER JOIN sclife_dev.accounts a ON ac.act_key = a.act_key
		INNER JOIN sclife_dev.individuals i ON a.act_primary_individual_id = i.indv_key
		#INNER JOIN sclife_dev.leads l ON a.act_key = l.lea_account_key
	ON q.ReferenceNumb = ac.arc_ref
#INNER JOIN p12life_dev.user u ON COALESCE(p.UnderAgentInitials, q.AgentInitials) = u.initials
INNER JOIN p12life_dev.user u ON q.AgentInitials = u.initials
INNER JOIN p12life_dev.party pt ON q.ClientID = pt.PartyID
LEFT JOIN p12life_dev.salesledger s ON q.ReferenceNumb = s.refer_numb
WHERE q.RequestDate >= DATE_ADD(CURDATE(), INTERVAL -2 MONTH);