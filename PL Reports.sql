/*
1) PL Reports - We need to create the ability to monitor and report on the cases that are within an agent's PL at any given in time. 
This will not be a historical report but will provide a snapshot of what is a specific agent's PL at any given point in time. 
Columns will include: Agent, Team, Account ID, PL Rule Name
*/

SELECT u.usr_key AS `Agent ID`, CONCAT(u.usr_first_name, ' ', u.usr_last_name) AS `Agent`,
	a.act_key AS `Account ID`, CONCAT(i.indv_first_name, ' ', i.indv_last_name) AS `Individual`,
    pr.prz_title AS `PL Rule Name`
FROM accounts a
INNER JOIN individuals i ON a.act_primary_individual_id = i.indv_key AND IFNULL(i.indv_delete_flag, 0) != 1
INNER JOIN leads l ON a.act_lead_primary_lead_key = l.lea_key AND IFNULL(l.lea_delete_flag, 0) != 1
#INNER JOIN campaigns c ON l.lea_cmp_id = c.cmp_id
INNER JOIN lead_prioritization_rules_map prm ON prm.przm_lea_key = l.lea_key
LEFT JOIN lead_prioritization_rules pr ON pr.prz_key = prm.przm_prz_key
INNER JOIN users u ON u.usr_key = 
	(CASE prm.przm_usr_type
		WHEN 1 THEN a.act_assigned_usr
		WHEN 2 THEN a.act_assigned_csr
		WHEN 3 THEN a.act_transfer_user
		WHEN 4 THEN a.act_ap_user
		WHEN 5 THEN a.act_op_user
		WHEN 6 THEN a.act_assigned_oa
		WHEN 7 THEN a.act_assigned_bdr
	END)
#INNER JOIN user_companies uc on uc.usr_key = u.usr_key AND a.act_company = uc.usr_companies
#LEFT JOIN user_multibusiness um ON um.umb_cpy_key = c.cmp_cpy_key AND um.umb_usr_key = u.usr_key
#LEFT JOIN status0 s0 ON l.lea_status = s0.sta_key
#LEFT JOIN status1 s1 ON l.lea_sub_status = s1.sta_key
#LEFT JOIN individual_statuses ist ON i.indv_individual_status_key = ist.ist_key
#LEFT JOIN states st ON COALESCE(i.indv_app_state, i.indv_state_Id) = st.sta_key
ORDER BY prm.przm_priority DESC, a.act_add_date DESC;